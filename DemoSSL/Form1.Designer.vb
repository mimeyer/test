Partial Class Form1

    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso (Not components Is Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub
#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify 
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.chkUseSSL = New System.Windows.Forms.CheckBox()
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.btnDisconnect = New System.Windows.Forms.Button()
        Me.lblState = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblSubscribe = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer()
        Me.SuspendLayout()
        '
        'chkUseSSL
        '
        Me.chkUseSSL.Location = New System.Drawing.Point(3, 3)
        Me.chkUseSSL.Name = "chkUseSSL"
        Me.chkUseSSL.Size = New System.Drawing.Size(181, 20)
        Me.chkUseSSL.TabIndex = 0
        Me.chkUseSSL.Text = "Use SSL"
        '
        'btnConnect
        '
        Me.btnConnect.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnConnect.Location = New System.Drawing.Point(190, 3)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(88, 20)
        Me.btnConnect.TabIndex = 1
        Me.btnConnect.Text = "Connect"
        '
        'btnDisconnect
        '
        Me.btnDisconnect.Enabled = False
        Me.btnDisconnect.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnDisconnect.Location = New System.Drawing.Point(284, 3)
        Me.btnDisconnect.Name = "btnDisconnect"
        Me.btnDisconnect.Size = New System.Drawing.Size(88, 20)
        Me.btnDisconnect.TabIndex = 2
        Me.btnDisconnect.Text = "Disconnect"
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Regular)
        Me.lblState.Location = New System.Drawing.Point(3, 44)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(369, 32)
        Me.lblState.Text = "Closed"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(3, 100)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(369, 20)
        Me.Label1.Text = "Last received message on topic kriticalsolution/second"
        '
        'lblSubscribe
        '
        Me.lblSubscribe.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Regular)
        Me.lblSubscribe.Location = New System.Drawing.Point(4, 120)
        Me.lblSubscribe.Name = "lblSubscribe"
        Me.lblSubscribe.Size = New System.Drawing.Size(369, 32)
        Me.lblSubscribe.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(376, 187)
        Me.Controls.Add(Me.lblSubscribe)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblState)
        Me.Controls.Add(Me.btnDisconnect)
        Me.Controls.Add(Me.btnConnect)
        Me.Controls.Add(Me.chkUseSSL)
        Me.Name = "Form1"
        Me.Text = "MQTT-Con via SSL"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents chkUseSSL As CheckBox
    Friend WithEvents btnConnect As Button
    Friend WithEvents btnDisconnect As Button
    Friend WithEvents lblState As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblSubscribe As Label
    Friend WithEvents Timer1 As Timer
#End Region
End Class
