Imports System
Imports System.Linq
Imports System.Collections.Generic
Imports System.Windows.Forms

NotInheritable Class Program
    ''' <summary>
    ''' The main entry point for the application.
    ''' </summary>
    <MTAThread>
    Shared Sub Main()
        Application.Run(New Form1())
    End Sub
End Class
