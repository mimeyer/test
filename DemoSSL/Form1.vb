Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports M2Mqtt.NetCF39
Imports M2Mqtt.NetCF39.Messages
Imports System.Security.Cryptography.X509Certificates

''' <summary>
''' MQTT-Connection-DEMO
''' </summary>
Partial Public Class Form1
    Inherits Form

    ''' <summary>
    ''' The client
    ''' </summary>
    Dim _MQTTClient As MqttClient

    ''' <summary>
    ''' End of app
    ''' </summary>
    Dim EndBit As Boolean = False

    ''' <summary>
    ''' Construcktor of form
    ''' </summary>
    Public Sub New()
        InitializeComponent()
    End Sub

    ''' <summary>
    ''' Closing form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Form1_Closing(sender As Object, e As CancelEventArgs) Handles MyBase.Closing
        EndBit = True
        If Not IsNothing(_MQTTClient) Then
            If _MQTTClient.IsConnected Then _MQTTClient.Disconnect()
        End If
        Timer1.Enabled = False
    End Sub

    ''' <summary>
    ''' Open connection
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnConnect_Click(sender As Object, e As EventArgs) Handles btnConnect.Click
        Try
            If chkUseSSL.Checked Then
                'Use SSL --> Not working --> This is your work! --> Here comes our actual way!!!!!!!!!!
                Dim myCert As X509Certificate
                Dim myStore As New X509Store(StoreName.My, StoreLocation.CurrentUser)
                myStore.Open(OpenFlags.MaxAllowed)
                Dim myResultsCA As X509Certificate2Collection = myStore.Certificates.Find(X509FindType.FindBySubjectName, "Mosquitto", False)
                If myResultsCA.Count = 0 Then
                    MsgBox("Client certificate not found!")
                    Exit Sub
                End If
                myCert = myResultsCA(0)
                myStore.Close()
                'This doesnt work because of no OpenSSL included!!! --> So the DllImports in Net/SslStream-Class faild when open connection!
                _MQTTClient = New MqttClient("solidblue.de", 8883, True, myCert, Nothing, MqttSslProtocols.TLSv1_2)
            Else
                'Connection without SSL
                _MQTTClient = New MqttClient("solidblue.de")
            End If

            'Handler, if connection lost
            AddHandler _MQTTClient.ConnectionClosed, AddressOf Client_ConnectionClosed
            'Handler, for receiving submitted topics
            AddHandler _MQTTClient.MqttMsgPublishReceived, AddressOf Client_MqttMsgPublishReceived

            'Do connect --> The access data, we created specifically for you. You can publish and subscribe everything on topic 'kriticalsolution/#' like you want to.
            Dim ConnectResponse As Byte = _MQTTClient.Connect("kritikalsolutions", "kritikalsolutions", "Ghj8#sTz%!")
            If ConnectResponse = 0 Then
                'Connect was OK
                btnConnect.Enabled = False
                btnDisconnect.Enabled = True
                lblState.Text = "Connected"
                'Subsribing
                SubscribeTestTopic()
                'Start publishing
                Timer1.Enabled = True
            Else
                'Connect was not successful
                'Error message
                MsgBox("Error-Code: " & ConnectResponse.ToString)
            End If


        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Disconnect
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnDisconnect_Click(sender As Object, e As EventArgs) Handles btnDisconnect.Click
        If Not IsNothing(_MQTTClient) Then
            _MQTTClient.Disconnect()
        End If
    End Sub

    ''' <summary>
    ''' Update Label in active thread.
    ''' </summary>
    ''' <param name="val"></param>
    Private Delegate Sub DUpdateDisplay(val As String)

    ''' <summary>
    ''' Receive submitted topic
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Client_MqttMsgPublishReceived(sender As Object, e As MqttMsgPublishEventArgs)
        If e.Topic = "kriticalsolution/second" Then
            'Get the message
            Dim MyMessage As String = Encoding.UTF8.GetString(e.Message, 0, (e.Message).Length)
            'Display the message
            Me.Invoke(New DUpdateDisplay(AddressOf OnUpdateDisplay), MyMessage)
        End If
    End Sub

    ''' <summary>
    ''' Update Label.
    ''' </summary>
    ''' <param name="val"></param>
    Private Sub OnUpdateDisplay(val As String)
        lblSubscribe.Text = val
    End Sub

    ''' <summary>
    ''' Update states on form in active thread
    ''' </summary>
    Private Delegate Sub DClientClosed()

    ''' <summary>
    ''' Connection lost
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Client_ConnectionClosed(sender As Object, e As EventArgs)
        If Not EndBit Then Me.Invoke(New DClientClosed(AddressOf OnClientClosed))
    End Sub

    ''' <summary>
    ''' Client has closed
    ''' </summary>
    Private Sub OnClientClosed()
        btnConnect.Enabled = True
        btnDisconnect.Enabled = False
        lblState.Text = "Closed"
        Timer1.Enabled = False
    End Sub

    ''' <summary>
    ''' Do subscription
    ''' </summary>
    Private Sub SubscribeTestTopic()
        _MQTTClient.Subscribe({"kriticalsolution/second"}, {0})
    End Sub

    ''' <summary>
    ''' Publish every second the actual second
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If Not IsNothing(_MQTTClient) Then
            If _MQTTClient.IsConnected Then
                'Publish with qosLevel = 0 (0..2 is possible --> 0 is like UDP and 2 is like TCP) and retain = false (true means server stores the published value)
                _MQTTClient.Publish("kriticalsolution/second", Encoding.UTF8.GetBytes(Now.Second.ToString), 0, False)
            Else
                Timer1.Enabled = False
            End If
        End If
    End Sub
End Class
